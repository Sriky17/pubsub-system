package com.incedo.pubsub.receiver.m3.util;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.incedo.pubsub.receiver.m3.model.DataPacket;

@FeignClient(value="feignDemo", url="http://localhost:8081/api/sender")
public interface ReceiverUtil {

	@PostMapping("/publish")
	public DataPacket publish(@RequestBody DataPacket sender);
}
