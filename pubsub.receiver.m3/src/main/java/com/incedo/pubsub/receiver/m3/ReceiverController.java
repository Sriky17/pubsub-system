package com.incedo.pubsub.receiver.m3;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.incedo.pubsub.receiver.m3.config.RedisMessageSubscriber;
import com.incedo.pubsub.receiver.m3.model.DataPacket;
import com.incedo.pubsub.receiver.m3.util.ReceiverUtil;



@RestController
@RequestMapping("/receiver")
public class ReceiverController {
	
	private static Logger logger= LoggerFactory.getLogger(ReceiverController.class);

	@Autowired
	private ReceiverUtil receiverUtil;
	
	
	@PostMapping("/publish")
	public DataPacket publish(@RequestBody DataPacket sender) {
		logger.info(">> publishing {}",sender);
//	      sender.setS_messageStatus("read");
		receiverUtil.publish(sender);
		return sender;
	}
	
	@GetMapping("/subscribe")
	public List<String> getMessage(){
		return RedisMessageSubscriber.messageList;
	}
}
