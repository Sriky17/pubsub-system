package com.incedo.pubsub.receiver.m3.config;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.redis.connection.Message;
import org.springframework.data.redis.connection.MessageListener;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.exc.StreamReadException;
import com.fasterxml.jackson.databind.DatabindException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.incedo.pubsub.receiver.m3.model.Receiver;
import com.incedo.pubsub.receiver.m3.model.DataPacket;

@Service
public class RedisMessageSubscriber implements MessageListener {
	

	@Qualifier("myobjectMapper")
	@Autowired
	ObjectMapper myobjectMapper;

	
	public static List<String> messageList= new ArrayList<>();
	
	@Override
	public void onMessage(Message message, byte[] pattern) {
		// TODO Auto-generated method stub;
		byte[] body=message.getBody();
		ObjectMapper myobjectMapper = new ObjectMapper();
		DataPacket receiver=null;
		try {
			receiver=myobjectMapper.readValue(body, DataPacket.class);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		receiver.setS_messageStatus("read");
		messageList.add(receiver.toString());
		System.out.println(receiver);
		
	}

}
