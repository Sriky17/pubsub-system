package com.incedo.pubsub.sender.m1.model;

import java.io.Serializable;

import org.springframework.data.redis.core.RedisHash;

@RedisHash("DataPacket")
public class DataPacket implements Serializable {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 515709997455755665L;
	private String s_author;
	private String s_message;
	private String s_messageStatus;
	public DataPacket() {
		super();
		// TODO Auto-generated constructor stub
	}
	public DataPacket(String s_author, String s_message, String s_messageStatus) {
		super();
		this.s_author = s_author;
		this.s_message = s_message;
		this.s_messageStatus = s_messageStatus;
	}
	public String getS_author() {
		return s_author;
	}
	public void setS_author(String s_author) {
		this.s_author = s_author;
	}
	public String getS_message() {
		return s_message;
	}
	public void setS_message(String s_message) {
		this.s_message = s_message;
	}
	public String getS_messageStatus() {
		return s_messageStatus;
	}
	public void setS_messageStatus(String s_messageStatus) {
		this.s_messageStatus = s_messageStatus;
	}
	@Override
	public String toString() {
		return "DataPacket [s_author=" + s_author + ", s_message=" + s_message + ", s_messageStatus=" + s_messageStatus
				+ "]";
	}
	
	
	
	
	
	
}
