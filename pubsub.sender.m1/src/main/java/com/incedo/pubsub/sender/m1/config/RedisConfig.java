package com.incedo.pubsub.sender.m1.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.listener.ChannelTopic;
import org.springframework.data.redis.listener.RedisMessageListenerContainer;
import org.springframework.data.redis.listener.adapter.MessageListenerAdapter;
import org.springframework.data.redis.serializer.GenericToStringSerializer;
import org.springframework.data.redis.serializer.Jackson2JsonRedisSerializer;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.incedo.pubsub.sender.m1.model.DataPacket;


@Component
@EnableCaching
public class RedisConfig {

	@Autowired
	private RedisConnectionFactory redisConnectionFactory;
	

	
//	@Bean
//	RedisMessageListenerContainer container(RedisConnectionFactory redisConnectionFactory, MessageListenerAdapter messageListenerAdapter) {
//		RedisMessageListenerContainer redisMessageListenerContainer = new RedisMessageListenerContainer();
//		redisMessageListenerContainer.setConnectionFactory(redisConnectionFactory);
//		redisMessageListenerContainer.addMessageListener(messageListenerAdapter, topic());
//		return redisMessageListenerContainer;
//	}
//	
//	@Bean
//	MessageListenerAdapter messageListenerAdapter() {
//		return new MessageListenerAdapter(new RedisMessageSubscriber(),"onMessage");
//	}
	
	@Bean
	public ChannelTopic topic() {
		return  new ChannelTopic("stackfortech");
	}
	
	
	@Bean
	RedisTemplate<String,Object> redisTemplate(RedisConnectionFactory redisConnectionFactory) {
		RedisTemplate<String,Object> redisTemplate = new RedisTemplate<>();
		redisTemplate.setConnectionFactory(redisConnectionFactory);
		redisTemplate.setValueSerializer(new Jackson2JsonRedisSerializer<>(DataPacket.class));
		return redisTemplate;
	}
	
	@Bean
	MessagePublisher messagePublisher() {
		return new RedisMessagePublisher(redisTemplate(redisConnectionFactory),topic());
		
	}
}
