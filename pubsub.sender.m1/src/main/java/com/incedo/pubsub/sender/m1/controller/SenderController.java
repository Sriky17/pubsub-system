package com.incedo.pubsub.sender.m1.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.incedo.pubsub.sender.m1.config.RedisMessagePublisher;
import com.incedo.pubsub.sender.m1.model.DataPacket;


@RestController
@RequestMapping("/api/sender")
public class SenderController {

	private static Logger logger= LoggerFactory.getLogger(SenderController.class);
	
	@Autowired
	private RedisMessagePublisher messagePublisher;
	
	
	@PostMapping("/publish")
	public DataPacket publish(@RequestBody DataPacket sender) {
		logger.info(">> publishing {}",sender);
		//sender.setS_messageStatus("read");
		messagePublisher.publish(sender);
		return sender;
	}
	
}
