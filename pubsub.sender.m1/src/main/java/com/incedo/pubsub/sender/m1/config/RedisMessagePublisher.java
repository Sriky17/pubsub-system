package com.incedo.pubsub.sender.m1.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.listener.ChannelTopic;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.incedo.pubsub.sender.m1.model.DataPacket;

@Component
public class RedisMessagePublisher implements MessagePublisher{

	@Autowired
	private RedisTemplate<String,Object> redisTemplate;
	@Autowired
	private ChannelTopic topic;
	
	public RedisMessagePublisher(RedisTemplate<String, Object> redisTemplate, ChannelTopic topic) {
		super();
		this.redisTemplate = redisTemplate;
		this.topic = topic;
	}

	@Override
	public void publish(DataPacket sender) {
		// TODO Auto-generated method stub
		ObjectMapper myobjectMapper = new ObjectMapper();
		String mesg = null;
		try {
			mesg = myobjectMapper.writeValueAsString(sender);
			redisTemplate.convertAndSend(topic.getTopic(), sender);
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}

}
