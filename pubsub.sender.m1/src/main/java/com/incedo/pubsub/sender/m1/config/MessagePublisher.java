package com.incedo.pubsub.sender.m1.config;

import com.incedo.pubsub.sender.m1.model.DataPacket;

public interface MessagePublisher {

	void publish(DataPacket message);
}
